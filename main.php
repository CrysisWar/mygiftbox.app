<?php

session_start();


/* pour le chargement automatique des classes dans vendor */
require_once 'vendor/autoload.php';

/* chargement des classes */
require_once 'src/mf/utils/ClassLoader.php';
$loader = new mf\utils\ClassLoader('src');
$loader->register() ;

/* alias */

use \mygiftboxapp\model\Categorie as Categorie;
use \mygiftboxapp\model\Coffret as Coffret;
use \mygiftboxapp\model\LogUrl as LogUrl;
use \mygiftboxapp\model\Paiement as Paiement;
use \mygiftboxapp\model\Prestation as Prestation;
use \mygiftboxapp\model\Utilisateur as Utilisateur;


$config = parse_ini_file("conf/config.ini");

/* une instance de connexion  */
$db = new Illuminate\Database\Capsule\Manager();

$db->addConnection( $config ); /* configuration avec nos paramètres */
$db->setAsGlobal();            /* visible de tout fichier */
$db->bootEloquent();           /* établir la connexion */

// ---------------------------------------------------------------------------------


$router = new \mf\router\Router();

$router->addRoute('accueil','/accueil','\mygiftboxapp\control\AccueilController','viewAccueil',\mygiftboxapp\auth\MygiftboxAuthentification::ACCESS_LEVEL_NONE);


$router->addRoute('mentions','/mentions','\mygiftboxapp\control\AccueilController','viewMentions',\mygiftboxapp\auth\MygiftboxAuthentification::ACCESS_LEVEL_NONE);
$router->addRoute('contact','/contact','\mygiftboxapp\control\AccueilController','viewContact',\mygiftboxapp\auth\MygiftboxAuthentification::ACCESS_LEVEL_NONE);
$router->addRoute('inscription','/inscription','\mygiftboxapp\control\InscriptionController', 'viewInscription',\mygiftboxapp\auth\MygiftboxAuthentification::ACCESS_LEVEL_NONE);

$router->addRoute('catalogue','/catalogue','\mygiftboxapp\control\CatalogueController', 'viewCatalogue',\mygiftboxapp\auth\MygiftboxAuthentification::ACCESS_LEVEL_NONE);
$router->addRoute('administration','/administration','\mygiftboxapp\control\AdministrationController', 'viewAdministration', \mygiftboxapp\auth\MygiftboxAuthentification::ACCESS_LEVEL_ADMIN);
$router->addRoute('ajout','/ajout','\mygiftboxapp\control\AdministrationController', 'viewAjout', \mygiftboxapp\auth\MygiftboxAuthentification::ACCESS_LEVEL_ADMIN);
$router->addRoute('modifier','/modifier','\mygiftboxapp\control\AdministrationController', 'viewModifier', \mygiftboxapp\auth\MygiftboxAuthentification::ACCESS_LEVEL_ADMIN);
$router->addRoute('supprimer','/supprimer','\mygiftboxapp\control\AdministrationController', 'viewSupprimer', \mygiftboxapp\auth\MygiftboxAuthentification::ACCESS_LEVEL_ADMIN);
$router->addRoute('moncoffret','/moncoffret','\mygiftboxapp\control\CoffretsController', 'viewMonCoffret', \mygiftboxapp\auth\MygiftboxAuthentification::ACCESS_LEVEL_NONE);

$router->addRoute('deconnexion','/deconnexion', '\mygiftboxapp\control\AccueilController','decoUser',\mygiftboxapp\auth\MygiftboxAuthentification::ACCESS_LEVEL_USER);
$router->addRoute('profil','/profil','\mygiftboxapp\control\ProfilController','viewProfil',\mygiftboxapp\auth\MygiftboxAuthentification::ACCESS_LEVEL_USER);

$router->setDefaultRoute('/accueil');

$router->addRoute('paiement','/paiement','\mygiftboxapp\control\PaiementController','viewPaiement',\mygiftboxapp\auth\MygiftboxAuthentification::ACCESS_LEVEL_USER);


$router->run();

