<?php
/**
 * Created by PhpStorm.
 * User: CrysisWar
 * Date: 08/11/2018
 * Time: 08:48
 */

namespace mygiftboxapp\auth;

use mf\auth\exception\AuthentificationException;
use mf\auth\Authentification;

class MygiftboxAuthentification extends \mf\auth\Authentification{

    const ACCESS_LEVEL_USER = 100;
    const ACCESS_LEVEL_ADMIN = 999;

    public function __construct()
    {
        parent::__construct();
    }


    public function createUser($nom, $prenom, $mail, $pass, $pass_ver, $level=self::ACCESS_LEVEL_USER){
        $ajout_user = new \mygiftboxapp\model\Utilisateur();

        $verif_user = $ajout_user::select()->where('mail','=',$mail);
        if($verif_user->first()){
            throw new AuthentificationException('Ce mail existe déjà, creation impossible.');
        }else if(strlen($pass)<8){
            throw new AuthentificationException('Le mot de passe doit être composé de plus de 8 caractères');
        }else if($pass !== $pass_ver){
            throw new AuthentificationException('Le mot de passe ne correspond pas.');
        }else{

            $pass_hash = $this->hashPassword($pass);

            $ajout_user::insert(
                ['nom' => $nom,
                'prenom' => $prenom,
                'mail' => $mail,
                'password' => $pass_hash,
                'access_level' => $level]
            );

        }
    }

    public function editUser($nom, $prenom, $mailEdit, $pass, $pass_ver){
        $edit_user = new \mygiftboxapp\model\Utilisateur();

        if($_SESSION['user_login'] !== $mailEdit) {
            $verif_user = $edit_user::select()->where('mail', '=', $mailEdit);
            if($verif_user->first()){
                throw new AuthentificationException('Ce mail existe déjà, modification impossible.');
            }else if(strlen($pass)<8){
                throw new AuthentificationException('Le mot de passe doit être composé de plus de 8 caractères');
            }else if($pass !== $pass_ver){
                throw new AuthentificationException('Le mot de passe ne correspond pas.');
            }else{

                $edit = $edit_user::select()->where('mail','=',$_SESSION['user_login']);
                $pass_hash = $this->hashPassword($pass);

                $edit->nom = $nom;
                $edit->prenom = $prenom;
                $edit->mail = $mailEdit;
                $edit->password = $pass_hash;
                $edit->save();
            }
        }else {
            if (strlen($pass) < 8) {
                throw new AuthentificationException('Le mot de passe doit être composé de plus de 8 caractères');
            } else if ($pass !== $pass_ver) {
                throw new AuthentificationException('Le mot de passe ne correspond pas.');
            } else {

                $edit = $edit_user::where('mail', '=', $_SESSION['user_login']);
                $pass_hash = $this->hashPassword($pass);

                $edit->nom = $nom;
                $edit->prenom = $prenom;
                $edit->mail = $mailEdit;
                $edit->password = $pass_hash;

                $edit->save();
            }

        }
    }




    public function loginUser($mail, $password){
        $recup_user = new \mygiftboxapp\model\Utilisateur();

        $verif_user = $recup_user::select()->where('mail','=',$mail)->first();

        if($verif_user){
            $auth = new Authentification();
            $auth->login($mail,$verif_user['password'],$password,$verif_user['access_level']);
        }else{
            throw new AuthentificationException('Ce mail n\'existe pas');
        }

    }

}

