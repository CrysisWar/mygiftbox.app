<?php

namespace mygiftboxapp\view;

use mf\utils\Toolbox;
use mf\router\Router;

class AdministrationView extends \mf\view\AbstractView
{

    public function __construct($data)
    {
        parent::__construct($data);

        array_push(self::$style_sheets,'normalize.css');
        array_push(self::$style_sheets,'style.css');
        array_push(self::$style_sheets,'fontawesome/css/all.css');

    }




    private function renderInside(){
        $body =  "
                    <section>
                        <article>Log des prestations:</article>
                        <article>
                            <div><p>Prestations XXXX supprimée par XXX le JJ/MM/YYYY a hh-mm-ss</p></div>
                        </article>
                    </section>
                    ";
        return $body;
    }


    protected function renderBody($selector=null){

        switch($selector){
            case 'administration':
                $render = TemplateView::renderHeader();
                $render .= $this->renderInside();
                break;

            case 'mentions':
                $render = TemplateView::renderHeader();
                $render .=
                    "<section><article>A REMPLIR</article></section>";
                break;

            case 'contact':
                $render = TemplateView::renderHeader();
                $render .=
                    "<section><article>A REMPLIR</article></section>";
                break;

            default:

                break;
        }

        return $render.TemplateView::renderFooter();

    }



}