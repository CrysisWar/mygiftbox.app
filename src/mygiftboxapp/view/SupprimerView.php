<?php

namespace mygiftboxapp\view;

use mf\utils\Toolbox;
use mf\router\Router;

class SupprimerView extends \mf\view\AbstractView
{

    public function __construct($data)
    {
        parent::__construct($data);

        array_push(self::$style_sheets,'normalize.css');
        array_push(self::$style_sheets,'style.css');
        array_push(self::$style_sheets,'fontawesome/css/all.css');

    }


    private function renderInside(){
        $body =  
        "<form method='post' action= ". Toolbox::urlLink('supprimer') .">
            
            <fieldset>
                <label for='nom-prestation'>Nom de la préstation :</label>
                    <select id='nom-prestation'>
                        <optgroup label='Nom de la prestation'>
                    ";
                        foreach ($this->data as $row) {
                            $body.= "<option>".$row->img."'|'".$row->nom. " | ID =" .$row->id."</option>"; 
                        };
                    $body.="
                        </optgroup>
                    </select>
            
                <label for='motif'>Motif de la suppression</label>
                    <textarea id='motif' name='motif'></textarea>
            </fieldset>

            <button type='submit'>Supprimer la prestation</button>

        </form>


                    

        ";
        return $body;
    }


    protected function renderBody($selector=null){

        switch($selector){
            case 'supprimer':
                $render = TemplateView::renderHeader();
                $render .= $this->renderInside();
                break;

            case 'mentions':
                $render = TemplateView::renderHeader();
                $render .=
                    '<section><article>A REMPLIR</article></section>';
                break;
                
            case 'contact':
                $render = TemplateView::renderHeader();
                $render .=
                    '<section><article>A REMPLIR</article></section>';
                break;

            default:

                break;
        }

        return $render.TemplateView::renderFooter();

    }
}