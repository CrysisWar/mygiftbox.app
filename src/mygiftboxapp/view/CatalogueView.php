<?php
/**
 * Created by PhpStorm.
 * User: CrysisWar
 * Date: 07/11/2018
 * Time: 15:45
 */

namespace mygiftboxapp\view;

use mf\utils\Toolbox;
use mf\router\Router;

class CatalogueView extends \mf\view\AbstractView
{

    public function __construct($data)
    {
        parent::__construct($data);

        array_push(self::$style_sheets,'normalize.css');
        array_push(self::$style_sheets,'style.css');
        array_push(self::$style_sheets,'fontawesome/css/all.css');

    }
	
	private function renderMenu(){
		$menu = "<section>
                        
			<article>
			<header><h2>Liste des catégories</header>";

			function suppr_accents($str, $encoding='utf-8')
			{
				$str = htmlentities($str, ENT_NOQUOTES, $encoding);
				$str = preg_replace('#&([A-za-z])(?:acute|grave|cedil|circ|orn|ring|slash|th|tilde|uml);#', '\1', $str);
				return $str ;
			}
			 
			foreach($this->data[0] as $row){
				$categorie = strtolower($row->nom) ;
				$categorie = suppr_accents($categorie) ; 
				$menu .= "<a href=". Toolbox::urlLink("catalogue") . '?categorie=' . $categorie.">". $row->nom ."</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					}

			$menu .= "      </article>      
						</section>";
		return $menu;
    }
	
    private function renderInside(){
        $body =  "Découvrez notre catalogue de prestations à travers différentes catégories !";          
        return $body;
    }

    private function renderInsideCategorie(){
        $body =  "<header style='text-align: center;'><h2>Catégorie</h2></header>
                    <section style='width:100%;border:1px solid black'>";

                        foreach($this->data[1] as $row){
                            $body .= "<article style='padding:10px;width:23%;margin:1%;border: 1px solid black;box-sizing:border-box;'>
                                        <header style='text-align:center;'>
                                            <h3>$row->nom</h3>
                                        </header>
                                        <div style='text-align: center;'>
                                            <img style='height: 150px;' src=". Toolbox::urlImg($row->img) ." alt='$row->img'>                                       
                                        </div>
										<p>Prix : <b>$row->prix</b> €</p>
										<div style='float:right;'>
											<a href=". Toolbox::urlLink("catalogue") . '?id=' . $row->id."> => Voir prestation <=</a>
										</div>
                                      </article>";
                        }

        $body .= "</section>";
        return $body;
    }
	
	private function renderInsidePrestation(){
        $body =  "<header style='text-align: center;'><h2>Prestation</h2></header>
                    <section style='margin:auto;width:40%;border:1px solid black'>";

                        foreach($this->data[1] as $row){
                            $body .= "<article style='padding:10px;width:100%;box-sizing:border-box;'>
                                        <header style='text-align:center;'>
                                            <h3>$row->nom</h3>
                                        </header>
                                        <div style='text-align: center;'>
                                            <img style='height: 150px;' src=". Toolbox::urlImg($row->img) ." alt='$row->img'>                                       
                                        </div>
										<p><u>Prix :</u> <b>$row->prix</b> €<br>
										<u>Description :</u> <br>
										$row->descr</p>

										<div style='float:right;'>
											<form method='post' action=". Toolbox::urlLink('catalogue') . "/panier>
												<input name='id_presta' type='hidden' value=". $row->id .">
												<input type='submit' value='Ajouter au panier'>
											</form>
										</div>
                                      </article>";
                        }

        $body .= "</section>";
        return $body;
    }
	
	private function renderInsidePanier(){
        $body =  "<header style='text-align: center;'><h2>Panier</h2></header>
                    <div style='display:flex;flex-flow: row nowrap;'><section style='margin:auto;width:40%;border:1px solid black'>";
			if (isset($_COOKIE["panier"])) {
			$panier = $_COOKIE["panier"] ;
			var_dump($panier);
                        foreach($this->data[1] as $row){
                            $body .= "<article style='padding:10px;width:100%;box-sizing:border-box;'>
                                        <header>
                                            <h3>$row->nom</h3>
                                        </header>
                                        <div style='text-align: center;float:left;'>
                                            <img style='height: 50px;' src=". Toolbox::urlImg($row->img) ." alt='$row->img'>                                       
                                        </div>
										<p><u>Prix :</u> <b>$row->prix</b></p>
										<div style='float:right;'>
											<form method='post' action=". Toolbox::urlLink('catalogue') . "/panier>
												<input name='id_presta' type='hidden' value=". $row->id .">
												<input type='submit' value='Retirer du panier'>
											</form>
										</div>
                                      </article>";
                        }}

        $body .= "</section>
	<section style='width:20%; height: 100px;border:1px solid black'>
		<form method='post' action=". Toolbox::urlLink('catalogue') . ">
			<input type='submit' value='Continuer les achats'>
		</form>
		<br>
		<form method='post' action=". Toolbox::urlLink('mescoffrets') . ">
			<input type='submit' value='Sauvegarder le panier'>
		</form>
		<br>
		<form method='post' action=". Toolbox::urlLink('catalogue') . "/valider>
			<input type='submit' value='Valider le panier'>
		</form>
	</section></div>";
        
	return $body;
    }

    protected function renderBody($selector=null){

        switch($selector){
           	case 'catalogue':
                $render = TemplateView::renderHeader();
				$render .= $this->renderMenu();
                $render .= $this->renderInside();
                break;

            case 'catalogueConn':
                $render = TemplateView::renderHeaderConn();
				$render .= $this->renderMenu();
                $render .= $this->renderInside();
                break;
            case 'categorie':
                $render = TemplateView::renderHeader();
				$render .= $this->renderMenu();
                $render .= $this->renderInsideCategorie();
                break;

            case 'categorieConn':
                $render = TemplateView::renderHeaderConn();
				$render .= $this->renderMenu();
                $render .= $this->renderInsideCategorie();    
                break;

            case 'prestation':
                $render = TemplateView::renderHeader();
				$render .= $this->renderMenu();
                $render .= $this->renderInsidePrestation();
                break;
            	case 'prestationConn':
                $render = TemplateView::renderHeaderConn();
		$render .= $this->renderMenu();
                $render .= $this->renderInsidePrestation();
                break;
		case 'panier':
		$render = TemplateView::renderHeader();
		$render .= $this->renderMenu();
                $render .= $this->renderInsidePanier();
		break;
		case 'panierConn':
		$render = TemplateView::renderHeaderConn();
		$render .= $this->renderMenu();
                $render .= $this->renderInsidePanier();
		break;
            case 'connectError':
                $render = TemplateView::renderHeaderError();
				$render .= $this->renderMenu();
                $render .= $this->renderInside();
                break;				
            default:
                break;
        }

        return $render.TemplateView::renderFooter();

    }

}
