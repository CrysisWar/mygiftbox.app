<?php
/**
 * Created by PhpStorm.
 * User: CrysisWar
 * Date: 07/11/2018
 * Time: 15:45
 */

namespace mygiftboxapp\view;

use mf\utils\Toolbox;
use mygiftboxapp\view\TemplateView;
use mf\router\Router;

class AccueilView extends \mf\view\AbstractView
{

    public function __construct($data)
    {
        parent::__construct($data);

        array_push(self::$style_sheets,'normalize.css');
        array_push(self::$style_sheets,'style.css');
        array_push(self::$style_sheets,'fontawesome/css/all.css');

    }




    private function renderInside(){
        $body =  "
                    <section>
                        
                        <article>
                        <header><h2>Les dernieres prestations ajoutées disponibles</h2></header>";

                        foreach($this->data[0] as $row){
                            $body .= "<article>
                                        <header>
                                            <h3>$row->nom</h3>
                                        </header>
                                        <div>
                                            <img src=". Toolbox::urlImg($row->img) ." alt='$row->img'>
                                            <p>$row->descr</p>
                                        </div>
                                      </article>";
                        }

        $body .= "      </article>
                        <article>
                            <header><h3>Découvrez la joie d'offrir une GiftBox !</h3></header>
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sed gravida tortor. Nullam ligula mauris, sodales in augue non, elementum luctus ipsum. Donec odio nisi, ornare sed libero sed, cursus laoreet sapien. Pellentesque a dignissim orci. Fusce ornare nisl ut mauris tempus sodales. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer ultrices cursus volutpat. Maecenas vitae ex eu ligula ultrices interdum id quis velit. Suspendisse malesuada cursus accumsan. Sed eget lacinia nibh, id dictum eros. Integer ligula ex, consequat nec sapien quis, semper eleifend risus. Vivamus consequat sagittis justo, eu ultricies quam venenatis non. </p></div>
                            <a type='button' href=". Toolbox::urlLink('catalogue') .">Créer votre GiftBox dès maintenant</a>
                        </article>
                    </section>
                    ";
        return $body;
    }


    private function renderInsideMention(){
        $body = "
        <section>
            <h1>Mentions Légales</h1>
            <article>
                <header><h2>Lorem Ipsum</h2></header>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tincidunt felis vitae ipsum molestie, nec vulputate tellus varius. Proin ac bibendum mauris, vitae pharetra felis. Donec neque mauris, commodo vel odio ac, viverra suscipit odio. Sed fermentum mauris at neque facilisis commodo. Donec sit amet aliquet ipsum. Curabitur imperdiet tincidunt aliquam. Nullam eu nibh sed ipsum pharetra interdum. Ut at lacus fringilla, dignissim felis sed, volutpat felis. Curabitur euismod felis vitae maximus aliquam. </p>
            </article>
            <article>
                <header><h2>Lorem Ipsum</h2></header>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tincidunt felis vitae ipsum molestie, nec vulputate tellus varius. Proin ac bibendum mauris, vitae pharetra felis. Donec neque mauris, commodo vel odio ac, viverra suscipit odio. Sed fermentum mauris at neque facilisis commodo. Donec sit amet aliquet ipsum. Curabitur imperdiet tincidunt aliquam. Nullam eu nibh sed ipsum pharetra interdum. Ut at lacus fringilla, dignissim felis sed, volutpat felis. Curabitur euismod felis vitae maximus aliquam. </p>
            </article>
                <article>
                <header><h2>Lorem Ipsum</h2></header>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tincidunt felis vitae ipsum molestie, nec vulputate tellus varius. Proin ac bibendum mauris, vitae pharetra felis. Donec neque mauris, commodo vel odio ac, viverra suscipit odio. Sed fermentum mauris at neque facilisis commodo. Donec sit amet aliquet ipsum. Curabitur imperdiet tincidunt aliquam. Nullam eu nibh sed ipsum pharetra interdum. Ut at lacus fringilla, dignissim felis sed, volutpat felis. Curabitur euismod felis vitae maximus aliquam. </p>
            </article>
            <article>
                <header><h2>Lorem Ipsum</h2></header>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tincidunt felis vitae ipsum molestie, nec vulputate tellus varius. Proin ac bibendum mauris, vitae pharetra felis. Donec neque mauris, commodo vel odio ac, viverra suscipit odio. Sed fermentum mauris at neque facilisis commodo. Donec sit amet aliquet ipsum. Curabitur imperdiet tincidunt aliquam. Nullam eu nibh sed ipsum pharetra interdum. Ut at lacus fringilla, dignissim felis sed, volutpat felis. Curabitur euismod felis vitae maximus aliquam. </p>
            </article>
        </section>
        ";
        return $body;
    }

    private function renderInsideConnect(){
        $body = "
        <section>
            <h1>Contact</h1>
            <article>
                <header><h2>Lorem Ipsum</h2></header>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tincidunt felis vitae ipsum molestie, nec vulputate tellus varius. Proin ac bibendum mauris, vitae pharetra felis. Donec neque mauris, commodo vel odio ac, viverra suscipit odio. Sed fermentum mauris at neque facilisis commodo. Donec sit amet aliquet ipsum. Curabitur imperdiet tincidunt aliquam. Nullam eu nibh sed ipsum pharetra interdum. Ut at lacus fringilla, dignissim felis sed, volutpat felis. Curabitur euismod felis vitae maximus aliquam. </p>
            </article>
            <article>
                <header><h2>Lorem Ipsum</h2></header>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tincidunt felis vitae ipsum molestie, nec vulputate tellus varius. Proin ac bibendum mauris, vitae pharetra felis. Donec neque mauris, commodo vel odio ac, viverra suscipit odio. Sed fermentum mauris at neque facilisis commodo. Donec sit amet aliquet ipsum. Curabitur imperdiet tincidunt aliquam. Nullam eu nibh sed ipsum pharetra interdum. Ut at lacus fringilla, dignissim felis sed, volutpat felis. Curabitur euismod felis vitae maximus aliquam. </p>
            </article>
                <article>
                <header><h2>Lorem Ipsum</h2></header>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tincidunt felis vitae ipsum molestie, nec vulputate tellus varius. Proin ac bibendum mauris, vitae pharetra felis. Donec neque mauris, commodo vel odio ac, viverra suscipit odio. Sed fermentum mauris at neque facilisis commodo. Donec sit amet aliquet ipsum. Curabitur imperdiet tincidunt aliquam. Nullam eu nibh sed ipsum pharetra interdum. Ut at lacus fringilla, dignissim felis sed, volutpat felis. Curabitur euismod felis vitae maximus aliquam. </p>
            </article>
            <article>
                <header><h2>Lorem Ipsum</h2></header>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tincidunt felis vitae ipsum molestie, nec vulputate tellus varius. Proin ac bibendum mauris, vitae pharetra felis. Donec neque mauris, commodo vel odio ac, viverra suscipit odio. Sed fermentum mauris at neque facilisis commodo. Donec sit amet aliquet ipsum. Curabitur imperdiet tincidunt aliquam. Nullam eu nibh sed ipsum pharetra interdum. Ut at lacus fringilla, dignissim felis sed, volutpat felis. Curabitur euismod felis vitae maximus aliquam. </p>
            </article>
        </section>
        ";
        return $body;
    }


    protected function renderBody($selector=null){

        switch($selector){
            case 'accueil':
                $render = TemplateView::renderHeader();
                $render .= $this->renderInside();
                break;
            case 'accueilConn':
                $render = TemplateView::renderHeaderConn();
                $render .= $this->renderInside();
                break;
            case 'connectError':
                $render = TemplateView::renderHeaderError();
                $render .= $this->renderInside();
                break;
            case 'mentions':
                $render = TemplateView::renderHeader();
                $render .= $this->renderInsideMention();
                break;
            case 'mentionsConn':
                $render = TemplateView::renderHeaderConn();
                $render .= $this->renderInsideMention();
                break;
            case 'connectMenError':
                $render = TemplateView::renderHeaderError();
                $render .= $this->renderInsideMention();
                break;
            case 'contact':
                $render = TemplateView::renderHeader();
                $render .= $this->renderInsideConnect();
                break;
            case 'contactConn':
                $render = TemplateView::renderHeaderConn();
                $render .= $this->renderInsideConnect();
                break;
            case 'inscription':
                $render = TemplateView::renderHeader();
                $render .= "<h3>Compte créé avec succes !</h3><h4>Vous pouvez maintenant vous connecter !</h4>";
                $render .= $this->renderInside();
                break;
            default:

                break;
        }

        return $render.TemplateView::renderFooter();

    }



}