<?php
/**
 * Created by PhpStorm.
 * User: CrysisWar
 * Date: 07/11/2018
 * Time: 15:45
 */

namespace mygiftboxapp\view;

use mf\utils\Toolbox;
use mf\router\Router;

class PaiementView extends \mf\view\AbstractView
{

    public function __construct($data)
    {
        parent::__construct($data);

        array_push(self::$style_sheets,'normalize.css');
        array_push(self::$style_sheets,'style.css');
        array_push(self::$style_sheets,'fontawesome/css/all.css');

    }





    private function renderInside(){
        $body =  "
                    <section>
                        
                        <article>
                        <header><h2>Paiement</h2></header>


            <i class='fab fa-cc-visa fa-3x' style = 'margin-right:20%;' ></i>
            <i class='fab fa-paypal fa-3x' style = 'margin-right: 20%;'></i>
            <i class='fab fa-cc-mastercard fa-3x' style = 'margin-right: 20%;'></i>
            <i class='fab fa-cc-amex fa-3x'></i><br><br>
            
            <form action = ". Toolbox::urlLink('paiement') ." method='post'> <label for='code'> code: </label><input type='text' name='code' id='code' placeholder = 'Carte valide: 123456789' <br><br>
             <label for='nom'> Nom Prénom: </label><input type='text' name='nom' id='nom' placeholder = 'Nom Prénom ' 
             <label for='cryp'> Cryptogramme: </label><input type='text' maxlength = '3' name='cryp' id='cryp' placeholder = 'XXX' <br><br>      
             <label for='date'> Date d'expiration: </label><input type='text' maxlength = '16' name='date' id='date' placeholder = '01/01/2000' <br>
             <INPUT TYPE=\"submit\" NAME=\"nom\" VALUE=\"Envoyer\"> </form>
                        </article>
                    </section>
                    ";
        return $body;
    }

    private function renderInsideReussite(){
        $body =  "
                    <section>
                        
                        <article>
                        <header><h2>Paiement</h2></header>
                        
                        <p> Paiement réussi </p> 
                        </article>
                         </section>";
        return $body;
    }

    private function renderInsideEchec(){
        $body =  "
                    <section>
                        
                        <article>
                        <header><h2>Paiement</h2></header>
                        
                        <p> Echec du paiement </p> 
                        </article>
                         </section>";
        return $body;
    }


    protected function renderBody($selector=null){

        switch($selector){
            case 'connecte':
                $render = TemplateView::renderHeaderConn();
                $render .= $this->renderInside();
                break;
            case 'accueilConn':
                $render = TemplateView::renderHeaderConn();
                $render .= $this->renderInside();
                break;
            case 'connectError':
                $render = TemplateView::renderHeaderError();
                $render .= $this->renderInside();
                break;
           
            case 'reussite':
                $render = TemplateView::renderHeaderConn();
                $render .= $this->renderInsideReussite();
                break;

            case 'echec':
                $render = TemplateView::renderHeaderConn();
                $render .= $this->renderInsideEchec();
                break;

            default:

                break;
        }

        return $render;

    }



}