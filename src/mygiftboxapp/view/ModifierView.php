<?php

namespace mygiftboxapp\view;

use mf\utils\Toolbox;
use mf\router\Router;

class ModifierView extends \mf\view\AbstractView
{

    public function __construct($data)
    {
        parent::__construct($data);

        array_push(self::$style_sheets,'normalize.css');
        array_push(self::$style_sheets,'style.css');
        array_push(self::$style_sheets,'fontawesome/css/all.css');

    }



    private function renderInside(){
        $body =  "
        <form method='post' action= ". Toolbox::urlLink('modifier') .">
            <fieldset>
                    <label for='image'>Image de la préstation :</label>
                    <input type='file' name='impageP' id='imageP'/>

                    <label for='nom-prestation'>Nom de la préstation :*</label>
                    <input type ='text' name ='nom' id='nom'

                    <label for='type-prestation'>Type de la préstation :</label>
                    <select id='type-prestation'>
                        <optgroup label='Type de la préstations'>
                                <option>Restauration</option>
                                <option>Hebergement</option>
                                <option>Attentions</option>
                                <option>Activites</option>
                        </optgroup>
                    </select>    
                

                <label for='ville'>Ville de déroulement*</label>
                    <input type='text' id='ville' name='ville'>
                <label for='description'>Description de la préstation</label>
                    <input type='text' id='description' name='description'>
                <label for='prix'>Prix de la prestation*</label>
                    <input type='text' id='prix' name='prix'>
                <label class='switch'>
                    Disponibilité : <!--Ajouter le css du bouton--!></label>
                    <input type='checkbox'> 
                    <span class='slider round'></span>
               
                <p>* Champs obligatoires</p>
            </fieldset>
            <button type='submit'>Supprimer la prestation</button>
        </form>";
        return $body;
    }


    protected function renderBody($selector=null){

        switch($selector){
            case 'modifier':
                $render = TemplateView::renderHeader();
                $render .= $this->renderInside();
                break;

            case 'mentions':
                $render = TemplateView::renderHeader();
                $render .=
                    '<section><article>A REMPLIR</article></section>';
                break;
                
            case 'contact':
                $render = TemplateView::renderHeader();
                $render .=
                    '<section><article>A REMPLIR</article></section>';
                break;

            default:

                break;
        }

        return $render.TemplateView::renderFooter();

    }
}