<?php

namespace mygiftboxapp\view;

use mf\utils\Toolbox;
use mf\router\Router;

class MonCoffretView extends \mf\view\AbstractView
{

    public function __construct($data)
    {
        parent::__construct($data);

        array_push(self::$style_sheets,'normalize.css');
        array_push(self::$style_sheets,'style.css');
        array_push(self::$style_sheets,'fontawesome/css/all.css');

    }


    private function renderInside(){
        $body =  "
        <h1>Mon Coffret</h1>

                        <h3>Offert par ".$this->data['0']['id_user']."<h3>

                        <form method='post' action='/cgi-bin/script_init.cgi'>
                         <input type='button' value='Rafraichir' id='refresh' />
                        </form>

                    ";
        return $body;
    }

     private function renderInsideAtt(){
     $datetime1= new DateTime(date('Y-m-d  H:i:s')) ;
                        $datetime2= new DateTime($this->data['0']['date_ouverture']);
                        $attente = $datetime1->diff($datetime2); 
                                $body =    "
        <h1>Mon Coffret</h1>

                        <h3>Offert par ".$this->data['0']['id_user']."<h3>

                        Temps restant avant ouverture :

                        ".date_default_timezone_set('Europe/Paris')."

                        

                        Heure d'ouverture :".$this->data['0']['date_ouverture']."

                        
                        ". $attente->format('%R%a days') ."



                        
                        

                    ";
        return $body;
    }

    private function renderInsideCli(){
        $body =  "
        <h1>Mon Coffret</h1>

                        <h3>Offert par ".$this->data['0']['id_user']."<h3>

                        Liste des prestations du coffret : ".$this->data['0']['id_presta']." 

                        Message de ".$this->data['0']['id_user']." : <label for='messages'>".$this->data['0']['message']."</label>
                            <textarea id='messages' name='messages'></textarea>

                        <label for='message'>Message de remerciement</label>
                            <textarea id='message' name='message'></textarea>
                            <button type='submit'>Envoyer le message</button>

                    ";
        return $body;
    }


    protected function renderBody($selector=null){

        switch($selector){
            case 'moncoffret':
                $render = TemplateView::renderHeader();
                $render .= $this->renderInside();
                break;

            case 'moncoffretConn' :
                $render = TemplateView::renderHeaderConn();
                $render .= $this->renderInsideConn();
                break;

            case 'moncoffretattente' :
                $render = TemplateView::renderHeader();
                $render .= $this->renderInsideAtt();
                break;

            case 'moncoffretattenteConn' :
                $render = TemplateView::renderHeaderConn();
                $render .= $this->renderInsideAtt();
                break;

            case 'moncoffretclic'  :
                $render = TemplateView::renderHeader();
                $render .= $this->renderInsideCli();
                break;

            case 'moncoffretclicCon'   :
                $render =  TemplateView::renderHeaderConn();
                $render .= $this->renderInsideCli();
                break;


            case 'mentions':
                $render =  TemplateView::renderHeader();
                $render .=
                    '<section><article>A REMPLIR</article></section>';
                break;
                
            case 'contact':
                $render =  TemplateView::renderHeader();
                $render .=
                    '<section><article>A REMPLIR</article></section>';
                break;

            default:

                break;
        }

        return $render.TemplateView::renderFooter();

    }
}