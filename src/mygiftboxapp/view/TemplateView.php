<?php
/**
 * Created by PhpStorm.
 * User: CrysisWar
 * Date: 07/11/2018
 * Time: 15:45
 */

namespace mygiftboxapp\view;

use mf\utils\Toolbox;
use mf\router\Router;

class TemplateView
{

    public function __construct()
    {
    }

    static function renderHeader(){       
	$header =
            "<header>
                <ul>

                    <li><a href=". Toolbox::urlLink('accueil') ."><img src=". Toolbox::urlImg("logo-small.png") ." alt='logo'></a></li>

                    <li>
			
                        <form method='post' action=". Toolbox::urlLink('accueil') .">
                            <fieldset>
                                <input type='email' id='email' name='email' placeholder='Entrez votre Email'>
                                <input type='password' id='password' name='password' placeholder='Entrez votre Mot de Passe'>
                                <button type='submit'>Se Connecter</button>
                                <p><a href=". Toolbox::urlLink('inscription') .">Pas encore de Compte ?</a></p>
                            </fieldset>
                        </form>

                    </li>
                </ul>
            </header>" ;
        
        return $header;
    }

    static function renderHeaderConn(){
        $header =
            "<header>
                <ul>
                    <li><a href=". Toolbox::urlLink('accueil') ."><img src=". Toolbox::urlImg("logo-small.png") ." alt='logo'></a></li>

                    <li>" . $_SESSION['user_login'] ."</li>
                    <li><i class=\"fas fa-user fa-3x\"></i>
                        <ul>
                            <li><a href=". Toolbox::urlLink('profil') .">Mon Profil</a></li>
                            <li><a href='#'>Mes GiftBoxes <i class=\"fas fa-gift\"></i></a></li>
                            <li><a href=". Toolbox::urlLink('deconnexion') .">Se Déconnecter <i class=\"fas fa-sign-out-alt\"></i></a></li>
                        </ul>
                    </li>
                    <li><a href=". Toolbox::urlLink('panier') ."><i class=\"fas fa-shopping fa-3x\"></i></a></li>
                </ul>
            </header>" ;
      
        return $header;
    }

    static function renderHeaderError(){
        $header =
            "<div>
            <header>
                <ul>

                    <li><a href=". Toolbox::urlLink('accueil') ."><img src=". Toolbox::urlImg("logo-small.png") ." alt='logo'></a></li>


                    <li>
                        <form method='post' action=". Toolbox::urlLink('accueil') .">
                            <fieldset>
                                <input type='email' id='email' name='email' placeholder='Entrez votre Email'>
                                <input type='password' id='password' name='password' placeholder='Entrez votre Mot de Passe'>
                                <button type='submit'>Se Connecter</button>
                                <p><a href=". Toolbox::urlLink('inscription') .">Pas encore de Compte ?</a></p>
                                <p><b>Erreur de connexion - Veuillez réessayer<b></p>
                            </fieldset>
                        </form>

                    </li>
                </ul>
            </header>
            </div>
            ";
        return $header;
    }

    static function renderFooter(){
        $footer = "
        <footer>
        <a href=". Toolbox::urlLink("mentions") .">Mentions Légales</a>
        <a href=". Toolbox::urlLink("contact") .">Contact</a>
        <p>Réalisé par AJKT, 2018</p>
        </footer>
        ";
        return $footer;
    }


}
