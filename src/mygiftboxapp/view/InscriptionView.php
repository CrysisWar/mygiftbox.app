<?php
/**
 * Created by PhpStorm.
 * User: CrysisWar
 * Date: 09/11/2018
 * Time: 03:42
 */

namespace mygiftboxapp\view;

use mf\utils\Toolbox;

class InscriptionView extends \mf\view\AbstractView
{

    public function __construct($data)
    {
        parent::__construct($data);
    }

    private function renderInside(){
        $body = "
        <div>
        <form method='post' action=". Toolbox::urlLink('inscription') .">
            <fieldset>
                <label for='nom'>Entrez votre nom*</label>
                    <input type='text' id='nom' name='nom'>
                <label for='prenom'>Entrez votre prénom*</label>
                    <input type='text' id='prenom' name='prenom'>
                <label for='mail'>Entrez votre mail*</label>
                    <input type='mail' id='email' name='email'>
                <label for='password'>Entrez votre mot de passe*</label>
                    <input type='password' id='password' name='password' placeholder='8 caractères minimum'>
                <label for='password_confirm'>Confirmation du mot de passe*</label>
                    <input type='password' id='password_confirm' name='password_confirm'>
                <p>* Champs obligatoires</p>
            </fieldset>
            <button type='submit'>S'inscrire</button>
        </form>
        <img src='#' alt='une image'>
        </div>
        ";
        return $body;
    }

    protected function renderBody($selector = null)
    {
        switch($selector){
            case 'inscription':
                $render = TemplateView::renderHeader();
                $render .= $this->renderInside();
                break;
            case 'deco':
                $render = TemplateView::renderHeader();
                $render .= "<h1>Pour créer un compte, veuillez d'abord vous déconnecter de ".$this->data."</h1>";
                $render .= $this->renderInside();
                break;
            case 'postnone':
                $render = TemplateView::renderHeader();
                $render .= "<h3>Veuillez remplir tous les champs, merci.</h3>";
                $render .= $this->renderInside();
                break;
            default:
                break;
        }
        return $render.TemplateView::renderFooter();
    }
}