<?php
/**
 * Created by PhpStorm.
 * User: CrysisWar
 * Date: 09/11/2018
 * Time: 18:13
 */

namespace mygiftboxapp\view;


use mf\utils\Toolbox;

class ProfilView extends \mf\view\AbstractView
{
    public function __construct($data)
    {
        parent::__construct($data);
    }



    private function renderInside(){
        $body ="
            <section>
                <article>
                    <header><h3>Mon profil</h3></header>
                    <div>
                        <form method='post' action=". Toolbox::urlLink('profil') .">
                            <fieldset>
                                <label for='nom'>Nom</label>
                                <input type='text' id='nom' name='nom' placeholder=". $this->data['nom'] ." value=". $this->data['nom'] .">
                                <label for='prenom'>Prenom</label>
                                <input id='prenom' name='prenom' placeholder=". $this->data['prenom'] ." value=". $this->data['prenom'] .">
                                <label id='mail'>Mail</label>
                                <input type='mail' id='mail' name='mail' placeholder=". $this->data['mail'] ." value=". $this->data['mail'] .">
                                <label id='password'>Mot de Passe</label>
                                <input type='password' id='password' name='password'>
                                <label id='password_confirmation'>Confirmation du mot de passe</label>
                                <input type='password' id='password_confitmation' name='password_confirmation'>
                                <button type='submit'>Confirmer</button>
                            </fieldset>
                        </form>
                    
                    </div>
                </article>
            </section>
        ";
        return $body;
    }



    protected function renderBody($selector = null)
    {

        switch($selector) {
            case 'connect':
                $render =  TemplateView::renderHeaderConn();
                $render .= $this->renderInside();
                break;
            case 'editError':
                $render =  TemplateView::renderHeaderConn();
                $render .= 'ceci est un test !"';
                break;
            case 'profilnonpost':
                $render =  TemplateView::renderHeaderConn();
                $render .= 'ceci est un test !"';
                break;
            default:
                break;
        }
        return $render. TemplateView::renderFooter();
    }
}