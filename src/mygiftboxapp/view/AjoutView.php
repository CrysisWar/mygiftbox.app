<?php

namespace mygiftboxapp\view;

use mf\utils\Toolbox;
use mf\router\Router;

class AjoutView extends \mf\view\AbstractView
{

    public function __construct($data)
    {
        parent::__construct($data);

        array_push(self::$style_sheets,'normalize.css');
        array_push(self::$style_sheets,'style.css');
        array_push(self::$style_sheets,'fontawesome/css/all.css');

    }



    private function renderInside(){
        $body =  "
                    <form method='post' action= ". Toolbox::urlLink('supprimer') ." enctype='multipart/form-data'>
            <fieldset>
                <input type='hidden' name='MAX_FILE_SIZE' value='15360' />
                    <label for='image'>Image de la prestation (JPG, PNG ou GIF | max. 15 Ko) :</label><br />
                    <input type='file' name='image' id='image'/><br />

                <label for='nompresta'>Nom de la prestation*</label>
                    <input type='text' id='nompresta' name='nompresta'>
                <label for='type-prestation'>Type de la préstation :</label>
                    <select id='type-prestation'>
                    <optgroup label='Type des prestations'>
                            <option>Restauration</option>
                            <option>Hebergement</option>
                            <option>Attentions</option>
                            <option>Activites</option>
                    </optgroup>
                    </select>

                <label for='ville'>Ville de déroulement*</label>
                    <input type='text' id='ville' name='ville'>
                <label for='description'>Description de la prestation</label>
                    <input type='text' id='description' name='description'>
                <label for='prix'>Prix de la prestation*</label>
                    <input type='text' id='prix' name='prix'>
                <p>* Champs obligatoires</p>
            </fieldset>
            <button type='submit'>Ajouter la prestation</button>
        </form>";
        return $body;
    }


    protected function renderBody($selector=null){

        switch($selector){
            case 'ajout':
                $render = TemplateView::renderHeader();
                $render .= $this->renderInside();
                break;

            case 'mentions':
                $render = TemplateView::renderHeader();
                $render .=
                    '<section><article>A REMPLIR</article></section>';
                break;
                
            case 'contact':
                $render = TemplateView::renderHeader();
                $render .=
                    '<section><article>A REMPLIR</article></section>';
                break;

            default:

                break;
        }

        return $render.TemplateView::renderFooter();

    }
}