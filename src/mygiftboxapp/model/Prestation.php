<?php 

namespace mygiftboxapp\model;

class Prestation extends \Illuminate\Database\Eloquent\Model
{

	protected $table = 'prestation';
	protected $primaryKey = 'id';
	public $timestamps = false;

	public function categories(){
	    return $this->belongsTo('Categorie','cat_id');
    }

    public function coffrets(){
	    return $this->belongsToMany('Coffret','prestaCoff','id_presta','id_coffret');
    }

}

