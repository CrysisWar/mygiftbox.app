<?php
/**
 * Created by PhpStorm.
 * User: CrysisWar
 * Date: 07/11/2018
 * Time: 15:21
 */

namespace mygiftboxapp\model;


class LogUrl extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'logurl';
    protected $primaryKey = 'id';
    public $timestamps = false;


    public function coffrets(){
        return $this->belongsTo('Coffret','id_coffret');
    }

}