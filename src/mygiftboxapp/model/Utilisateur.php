<?php 

namespace mygiftboxapp\model;

class Utilisateur extends \Illuminate\Database\Eloquent\Model
{

	protected $table = 'utilisateur';
	protected $primaryKey = 'id';
	public $timestamps = false;

	public function coffrets(){
	    return $this->hasMany('Coffret','id_user');
    }


}



