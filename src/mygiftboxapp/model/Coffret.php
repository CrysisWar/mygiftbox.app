<?php 

namespace mygiftboxapp\model;

class Coffret extends \Illuminate\Database\Eloquent\Model
{

	protected $table = 'coffret';
	protected $primaryKey = 'id';
	public $timestamps = false;

	public function prestations(){
	    return $this->belongsToMany('Prestation','prestaCoff','id_coffret','id_presta');
    }

    public function utilisateur(){
	    return $this->belongsTo('Utilisateur','id_user');
    }

    public function logurl(){
	    return $this->hasMany('LogUrl','id_coffret');
    }

    public function paiement(){
	    return $this->hasMany('Paiement','id_coffret');
    }

}
