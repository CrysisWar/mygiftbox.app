<?php 

namespace mygiftboxapp\model;

class Paiement extends \Illuminate\Database\Eloquent\Model
{

	protected $table = 'paiement';
	protected $primaryKey = 'id';
	public $timestamps = true;

	public function coffret(){
	    return $this->belongsTo('Coffret','id_coffret');
    }

}



