<?php
/**
 * Created by PhpStorm.
 * User: CrysisWar
 * Date: 09/11/2018
 * Time: 17:23
 */

namespace mygiftboxapp\control;

use mf\utils\HttpRequest;
use mygiftboxapp\model\Utilisateur;
use mygiftboxapp\view\ProfilView;
use mygiftboxapp\auth\MygiftboxAuthentification;
use mf\auth\exception\AuthentificationException;

class ProfilController extends \mf\control\AbstractController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function viewProfil()
    {

        $requests = new HttpRequest();

        $req = new Utilisateur();
        $reqUser = $req::select()->where('mail','=',$_SESSION['user_login']);

        if ($requests->method === 'get') {
                $vue = new ProfilView($reqUser->first());
                return $vue->render('connect');

        } else {
            if (!empty($requests->post['nom'])
                && !empty($requests->post['prenom'])
                && !empty($requests->post['mail'])
                && !empty($requests->post['password'])
                && !empty($requests->post['password_confirmation'])) {

                try {
                    $creation = new MygiftboxAuthentification();
                    $creation->EditUser(
                        $requests->post['nom'],
                        $requests->post['prenom'],
                        $requests->post['mail'],
                        $requests->post['password'],
                        $requests->post['password_confirmation']);
                } catch (AuthentificationException $e) {
                    $vue = new ProfilView($e->getMessage());
                    return $vue->render('editError');
                }
                $vue = new ProfilView($reqUser->first());
                return $vue->render('connect');

            }else{
                var_dump($_POST);
                $vue = new ProfilView($reqUser->first());
                return $vue->render('profilnonpost');
            }


        }
    }
}