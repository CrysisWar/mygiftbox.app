<?php

namespace mygiftboxapp\control;


use mf\auth\Authentification;
use mf\auth\exception\AuthentificationException;
use mf\utils\HttpRequest;
use mf\router\Router;
use mygiftboxapp\model\Categorie;
use mygiftboxapp\model\Prestation;
use mygiftboxapp\view\CatalogueView;
use mygiftboxapp\auth\MygiftboxAuthentification;

class CatalogueController extends \mf\control\AbstractController {


    /* Constructeur :
     *
     * Appelle le constructeur parent
     *
     * c.f. la classe \mf\control\AbstractController
     *
     */

    public function __construct(){
        parent::__construct();
    }


    public function viewCatalogue(){

        $requests = new HttpRequest();
        $new_presta = new Prestation();
        $new_categ = new Categorie();


        $requeteCateg = $new_categ::select() ;

        $requeteCateg = $new_categ::select() ;


        if($requests->method === 'get'){

            if(isset($_GET['categorie'])){
                $categorie = $_GET['categorie'] ;
                $categories = array (	'attention'=> 1,
                    'activite' => 2,
                    'restauration' => 3,
                    'hebergement'=> 4	);

                if (array_key_exists($categorie, $categories)){
                    $requetePresta = $new_presta::select()
                        ->where('cat_id','=',$categories[$categorie]);
                }

                $vue = new CatalogueView(array($requeteCateg->get(), $requetePresta->get()));

                if(!isset($_SESSION['user_login'])) {
                    return $vue->render('categorie');
                }else{
                    return $vue->render('categorieConn');
                }
            }
            else if(isset($_GET['id'])){
                $id_presta = $_GET['id'] ;
                $requetePresta = $new_presta::select()
                    ->where('id','=',$id_presta);

                $vue = new CatalogueView(array($requeteCateg->get(), $requetePresta->get()));

                if(!isset($_SESSION['user_login'])) {
                    return $vue->render('prestation');
                }else{
                    return $vue->render('prestationConn');
                }
            }
            else{
                $requetePresta = $new_presta::select();
                $vue = new CatalogueView(array($requeteCateg->get(), $requetePresta->get()));

                if(!isset($_SESSION['user_login'])) {
                    return $vue->render('catalogue');
                }else{
                    return $vue->render('catalogueConn');
                }
            }

        }else{
            if(isset($_POST['email']) && isset($_POST['password'])){

                try{
                    $requetePresta = $new_presta::select();
                    $auth = new MygiftboxAuthentification();
                    $auth->loginUser($_POST['email'],$_POST['password']);


                    $vue = new CatalogueView(array($requeteCateg->get(), $requetePresta->get()));
                    return $vue->render('catalogueConn');


                }catch(AuthentificationException $e){
                    $vue = new CatalogueView(array($requeteCateg->get(),$e->getMessage()));
                    return $vue->render('connectError');
                }

            }
        }
    }


    public function viewPanier(){

        $requests = new HttpRequest();
        $new_presta = new Prestation();
        $new_categ = new Categorie();

        $requeteCateg = $new_categ::select() ;

        if($requests->method === 'get'){
            $requetePresta = $new_presta::select();
            $vue = new CatalogueView(array($requeteCateg->get(), $requetePresta->get()));

            if(!isset($_SESSION['user_login'])) {
                return $vue->render('panier');
            }else{
                return $vue->render('panierConn');
            }

        }else if(isset($_POST['id_presta'])){
            $id_presta = $_POST['id_presta'] ;
            if (isset($_COOKIE["panier"])) {
                $panier = $_COOKIE["panier"] ;
                array_push($panier, $id_presta);
                setcookie("panier", $panier, 0);}
            else{
                $panier = array() ;
                array_push($panier, $id_presta);
                setcookie("panier", $panier, 0);
            }

            $requetePresta = $new_presta::select();

            $vue = new CatalogueView(array($requeteCateg->get(), $requetePresta->get()));

            if(!isset($_SESSION['user_login'])) {
                return $vue->render('panier');
            }else{
                return $vue->render('panierConn');
            }

        }else if(isset($_POST['email']) && isset($_POST['password'])){
            $requetePresta = $new_presta::select();

            try{
                $auth = new MygiftboxAuthentification();
                $auth->loginUser($_POST['email'],$_POST['password']);

                $vue = new CatalogueView(array($requeteCateg->get(), $requetePresta->get()));
                return $vue->render('panierConn');


            }catch(AuthentificationException $e){
                $vue = new CatalogueView(array($requeteCateg->get(),$e->getMessage()));
                return $vue->render('connectError');
            }

        }

    }

    public function decoUser(){

        if(isset($_SESSION['user_login'])){
            $logUser = new Authentification();
            $logUser->logout();
        }
        $vue = new router();
        $vue->executeRoute('/accueil');

    }

}
