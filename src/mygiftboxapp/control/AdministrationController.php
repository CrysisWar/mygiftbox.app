<?php

namespace mygiftboxapp\control;

use mygiftboxapp\model\Prestation;
use mf\utils\ClassLoader;
use mf\utils\HttpRequest;
use mf\utils\Toolbox;
use mygiftboxapp\auth\MygiftboxAuthentification;
use \mygiftboxapp\view\AdministrationView;

class AdministrationController extends \mf\control\AbstractController {

    /* Constructeur :
     * 
     * Appelle le constructeur parent
     *
     * c.f. la classe \mf\control\AbstractController
     * 
     */
    
    public function __construct(){
        parent::__construct();
    }
   
    public function viewAdministration(){
        $vue = new AdministrationView(null);
            return $vue->render('administration');
        }

    public function viewAjout(){
        $vue = new \mygiftboxapp\view\AjoutView(null);
            return $vue->render('ajout'); 

            if( !empty($requests->post['image'])
                || !empty($requests->post['nompresta'])
                || !empty($requests->post['ville'])
                || !empty($requests->post['prix'])
                || !empty($requests->post['description'])){

                $vue = new AdministrationView(null);
                return $vue->render('postnone');
            }
    }

    public function viewSupprimer(){
        $requests = new HttpRequest();
           $requete = new Prestation();
             $requeteView = $requete::select();

             // $requeteView2 = $requete::where()
             // $requeteView2 = $requete::where()

              if ( $requests->method === 'get'){
               $vue = new \mygiftboxapp\view\SupprimerView($requeteView->get());
            return $vue->render('supprimer');

           //   }else {
              }else {
               // $vue = new \mygiftboxapp\view\SupprimerView();
              //  return $vue->render('supprimer');
              //}
    }}

    public function viewModifier(){
        $vue = new \mygiftboxapp\view\ModifierView(null);
            return $vue->render('modifier');
    }

}
