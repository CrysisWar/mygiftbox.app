<?php

namespace mygiftboxapp\control;


use mf\auth\Authentification;
use mf\auth\exception\AuthentificationException;
use mf\utils\HttpRequest;
use mygiftboxapp\model\Categorie;
use mygiftboxapp\model\Paiement;
use mf\router\Router;
use mygiftboxapp\view\PaiementView;
use mygiftboxapp\model\Prestation;
use mygiftboxapp\auth\MygiftboxAuthentification;

class PaiementController extends \mf\control\AbstractController
{


    /* Constructeur :
     * 
     * Appelle le constructeur parent
     *
     * c.f. la classe \mf\control\AbstractController
     * 
     */

    public function __construct()
    {
        parent::__construct();
    }


    public function viewPaiement()
    {


        $requests = new HttpRequest();

        $new_paiement = new Paiement();
        if ($requests->method === 'get') {

            $vue = new PaiementView(null);

            if (!isset($_SESSION['user_login'])) {
                return $vue->render('non_connecte');
            } else {
                return $vue->render('connecte');
            }
        } else if (isset($_POST['email']) && isset($_POST['password'])) {

            /*$_POST['id_coffret'];
            $_POST['message'];
            $_POST['montant'];
            $_POST['date_paiement'];
            $_POST['mode'];

            $requetePaiement = $new_paiement::insert()
                ->values($_POST['id_coffret'], $_POST['mode'], true, $_POST['message'], $_POST['montant'], $_POST['date_paiement']);
            */



                try {
                    $auth = new MygiftboxAuthentification();
                    $auth->loginUser($_POST['email'], $_POST['password']);

                    $vue = new PaiementView(null);
                    return $vue->render('connecte');


                } catch (AuthentificationException $e) {
                    $vue = new PaiementView(array($e->getMessage()));
                    return $vue->render('non_connecte');
                }
        }
        else if (isset($_POST['code'])){
            if ($_POST['code'] == '123456789'){
                $vue = new PaiementView(null);
                return $vue->render('reussite');
            }
            else {
                $vue = new PaiementView(null);
                return $vue->render('echec');
            }
        }

    }



   /* public function viewMentions(){

        $requests = new HttpRequest();

        if($requests->method === 'get'){

            $vue = new PaiementView(null);

            if(!isset($_SESSION['user_login'])) {
                return $vue->render('mentions');
            }else{
                return $vue->render('mentionsConn');
            }

        }else{
            if(isset($_POST['email']) && isset($_POST['password'])){

                try{
                    $auth = new MygiftboxAuthentification();
                    $auth->loginUser($_POST['email'],$_POST['password']);

                    $vue = new PaiementView(null);
                    return $vue->render('mentionsConn');


                }catch(AuthentificationException $e){
                    $vue = new PaiementView(array(null,$e->getMessage()));
                    return $vue->render('connectMenError');
                }

            }
        }

    }

    public function viewContact(){


        $requests = new HttpRequest();

        if($requests->method === 'get'){

            $vue = new PaiementView(null);

            if(!isset($_SESSION['user_login'])) {
                return $vue->render('contact');
            }else{
                return $vue->render('contactConn');
            }

        }else{
            if(isset($_POST['email']) && isset($_POST['password'])){

                try{
                    $auth = new MygiftboxAuthentification();
                    $auth->loginUser($_POST['email'],$_POST['password']);

                    $vue = new PaiementView(null);
                    return $vue->render('ContactConn');


                }catch(AuthentificationException $e){
                    $vue = new PaiementView(array(null,$e->getMessage()));
                    return $vue->render('contactMenError');
                }

            }
        }

    } */

    public function decoUser(){

        if(isset($_SESSION['user_login'])){
            $logUser = new Authentification();
            $logUser->logout();
        }
        $vue = new router();
        $vue->executeRoute('/accueil');

    }

}
