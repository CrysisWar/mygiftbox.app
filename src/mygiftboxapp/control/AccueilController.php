<?php

namespace mygiftboxapp\control;


use mf\auth\Authentification;
use mf\auth\exception\AuthentificationException;
use mf\utils\HttpRequest;
use mygiftboxapp\model\Categorie;
use mf\router\Router;
use mygiftboxapp\view\AccueilView;
use mygiftboxapp\model\Prestation;
use mygiftboxapp\auth\MygiftboxAuthentification;

class AccueilController extends \mf\control\AbstractController {


    /* Constructeur :
     * 
     * Appelle le constructeur parent
     *
     * c.f. la classe \mf\control\AbstractController
     * 
     */
    
    public function __construct(){
        parent::__construct();
    }

    
    public function viewAccueil(){

        $requests = new HttpRequest();

        $new_presta = new Prestation();


        $requetePresta = $new_presta::select()
            ->where('disponibilite','=',1)
            ->orderBy('prestation.id', 'DESC')->limit(3);


        if($requests->method === 'get'){

            $vue = new AccueilView(array($requetePresta->get()));

            if(!isset($_SESSION['user_login'])) {
                return $vue->render('accueil');
            }else{
                return $vue->render('accueilConn');
            }

        }else{
            if(isset($_POST['email']) && isset($_POST['password'])){

                try{
                    $auth = new MygiftboxAuthentification();
                    $auth->loginUser($_POST['email'],$_POST['password']);

                    $vue = new AccueilView(array($requetePresta->get()));
                    return $vue->render('accueilConn');


                }catch(AuthentificationException $e){
                    $vue = new AccueilView(array($requetePresta->get(),$e->getMessage()));
                    return $vue->render('connectError');
                }

            }
        }


    }


    public function viewMentions(){

        $requests = new HttpRequest();

        if($requests->method === 'get'){

            $vue = new AccueilView(null);

            if(!isset($_SESSION['user_login'])) {
                return $vue->render('mentions');
            }else{
                return $vue->render('mentionsConn');
            }

        }else{
            if(isset($_POST['mail']) && isset($_POST['password'])){

                try{
                    $auth = new MygiftboxAuthentification();
                    $auth->loginUser($_POST['mail'],$_POST['password']);

                    $vue = new AccueilView(null);
                    return $vue->render('mentionsConn');


                }catch(AuthentificationException $e){
                    $vue = new AccueilView(array(null,$e->getMessage()));
                    return $vue->render('connectMenError');
                }

            }
        }

    }

    public function viewContact(){


        $requests = new HttpRequest();

        if($requests->method === 'get'){

            $vue = new AccueilView(null);

            if(!isset($_SESSION['user_login'])) {
                return $vue->render('contact');
            }else{
                return $vue->render('contactConn');
            }

        }else{
            if(isset($_POST['email']) && isset($_POST['password'])){

                try{
                    $auth = new MygiftboxAuthentification();
                    $auth->loginUser($_POST['email'],$_POST['password']);

                    $vue = new AccueilView(null);
                    return $vue->render('ContactConn');


                }catch(AuthentificationException $e){
                    $vue = new AccueilView(array(null,$e->getMessage()));
                    return $vue->render('contactMenError');
                }

            }
        }

    }

    public function decoUser(){

        if(isset($_SESSION['user_login'])){
            $logUser = new Authentification();
            $logUser->logout();
        }
        $this->viewAccueil();

    }

}
