<?php
/**
 * Created by PhpStorm.
 * User: CrysisWar
 * Date: 09/11/2018
 * Time: 02:54
 */

namespace mygiftboxapp\control;

use mf\auth\exception\AuthentificationException;
use mf\utils\HttpRequest;
use mygiftboxapp\model\Prestation;
use mf\utils\Toolbox;
use mygiftboxapp\view\InscriptionView;
use mygiftboxapp\view\AccueilView;
use mygiftboxapp\auth\MygiftboxAuthentification;


class InscriptionController extends \mf\control\AbstractController {


    public function __construct()
    {
        parent::__construct();
    }

    public function viewInscription(){

        $requests = new HttpRequest();

        if($requests->method === 'get'){
            if(isset($_SESSION['user_login'])){
                $vue = new InscriptionView($_SESSION['user_login']);
                return $vue->render('deco');
            }else{
                $vue = new InscriptionView(null);
                return $vue->render('inscription');
            }
        }else{
            if( !empty($requests->post['nom'])
                && !empty($requests->post['prenom'])
                && !empty($requests->post['mail'])
                && !empty($requests->post['password'])
                && !empty($requests->post['password_confirm'])) {

                try{
                    $creation = new MygiftboxAuthentification();
                    $creation->createUser(
                        $requests->post['nom'],
                        $requests->post['prenom'],
                        $requests->post['mail'],
                        $requests->post['password'],
                        $requests->post['password_confirm']);
                }
                catch(AuthentificationException $e){
                    $vue = new InscriptionView($e->getMessage());
                    return $vue->render('inscriptError');

                }


                $new_presta = new Prestation();
                $requetePresta = $new_presta::select()
                    ->join('categorie','prestation.cat_id','=','categorie.id')
                    ->orderBy('prestation.id', 'DESC')
                    ->limit(3);

                $vue = new AccueilView(array($requetePresta->get()));
                return $vue->render('inscription');

            }else {
                $vue = new InscriptionView(null);
                return $vue->render('postnone');

            }
        }




    }

}
