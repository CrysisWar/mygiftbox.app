<?php
/**
 * Created by PhpStorm.
 * User: CrysisWar
 * Date: 24/09/2018
 * Time: 14:40
 */

namespace mf\utils;


class ClassLoader
{

    private $prefix;

    function __construct($prefix)
    {
        $this->prefix = $prefix.'\\';
    }

    function loadClass($string)
    {
        $string = str_replace('\\', DIRECTORY_SEPARATOR, $this->prefix.$string.'.php');
        if(file_exists($string)){
            require_once($string);
        }
    }

    function register() {
        spl_autoload_register([$this,"loadClass"]);
    }

}
