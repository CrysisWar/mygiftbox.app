<?php
/**
 * Created by PhpStorm.
 * User: CrysisWar
 * Date: 07/11/2018
 * Time: 16:34
 */

namespace mf\utils;


class Toolbox
{

    public static function urlImg($img){

        $var = 'http://localhost/mygiftbox.app/html/img/'.$img;
        $var = strtr($var,'\\', DIRECTORY_SEPARATOR);
        return $var;
    }

    public static function urlLink($url){
        $var = 'http://localhost/mygiftbox.app/main.php/'.$url;
        return $var;
    }

    public static function urlCss($css){
        $var = 'http://localhost/mygiftbox.app/html/css/'.$css;
        return $var;
    }

}