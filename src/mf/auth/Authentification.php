<?php

namespace mf\auth;

use mf\auth\exception\AuthentificationException;

class Authentification extends AbstractAuthentification {

    protected $logged_in;
    protected $user_login;
    protected $access_level;

    public function __construct()
    {
        if(isset($_SESSION['user_login'])){

            $this->user_login = $_SESSION['user_login'];
            $this->access_level = $_SESSION['access_level'];

            $this->logged_in = true;
        }else{
            $this->user_login = null;
            $this->access_level = self::ACCESS_LEVEL_NONE;
            $this->logged_in = false;
        }
    }

    public function updateSession($username,$level){
        $_SESSION['user_login'] = $username;
        $_SESSION['access_level'] = $level;
        $this->logged_in = true;
    }

    public function logout(){
        unset($_SESSION['user_login']);
        unset($_SESSION['access_right']);
        $this->user_login = null;
        $this->access_level = null;
        $this->logged_in = false;
    }

    public function checkAccessRight($requested){
        if($requested > $this->access_level){
            return false;
        }else{
            return true;
        }
    }

    public function login($username, $db_pass, $given_pass, $level)
    {
        $verif = new Authentification();
        if(!$verif->verifyPassword($given_pass,$db_pass)){
            throw new AuthentificationException('Le mot de passe ne correspond pas');
        }else{
            $verif->updateSession($username,$level);
        }
    }

    public function hashPassword($password)
    {
        return password_hash($password,PASSWORD_DEFAULT);
    }

    public function verifyPassword($password, $hash)
    {
        return password_verify($password,$hash);
    }
}