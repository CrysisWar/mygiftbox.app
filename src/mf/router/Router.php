<?php

namespace mf\router;

use mf\auth\Authentification;

class Router extends \mf\router\AbstractRouter {


	/* Constructeur :
	* 
	* Appelle le constructeur parent
	*
	* c.f. la classe \mf\router\AbstractRouter
	* 
	*/

	public function __construct(){
		parent::__construct();
	}

	public function run(){
		/*
		* Méthode run : execute une route en fonction de la requête 
		*    (la requête est récupérée dans l'atribut $http_req)
		*
		* Algorithme :
		* 
		* - l'URL de la route est stockée dans l'attribut $path_info de 
		*         $http_request
		*   Et si une route existe dans le tableau $route sous le nom $path_info
		*     - créer une instance du controleur de la route
		*     - exécuter la méthode de la route 
		* - Sinon 
		*     - exécuter la route par défaut : 
		*        - créer une instance du controleur de la route par défault
		*        - exécuter la méthode de la route par défault
		* 
		*/
		
		$reqUrl = $this->http_req->path_info ;

		$auth = new Authentification();

		if (array_key_exists($reqUrl, self::$routes) && ($auth->checkAccessRight(self::$routes[$reqUrl][2]))){
			$controllerName = self::$routes[$reqUrl][0] ;
			$methodName = self::$routes[$reqUrl][1] ;
		}
		else {
			$defaultRoute = self::$routes[self::$aliases['default']] ;
			$controllerName = $defaultRoute[0] ;
			$methodName = $defaultRoute[1] ;
		}

		$controller = $controllerName ;
		$controller = new $controller();
		$controller->$methodName();
	}


	public function executeRoute($route){
		
		$curRoute = new self::$routes[$route][0] ;
		$method = self::$routes[$route][1] ;
		$curRoute->$method;	

	}

	public function urlFor($route_name, $param_list=[]){
		/*
		* Méthode urlFor : retourne l'URL d'une route depuis son alias 
		* 
		* Paramètres :
		* 
		* - $route_name (String) : alias de la route
		* - $param_list (Array) optionnel : la liste des paramètres si l'URL prend
		*          de paramètre GET. Chaque paramètre est représenté sous la forme
		*          d'un tableau avec 2 entrées : le nom du paramètre et sa valeur  
		*
		* Algorthme:
		*  
		* - Depuis le nom du scripte et l'URL stocké dans self::$routes construire 
		*   l'URL complète 
		* - Si $param_list n'est pas vide 
		*      - Ajouter les paramètres GET a l'URL complète    
		* - retourner l'URL
		*
		*/

		$reqUrl = $this->http_req->script_name ;
		$route = self::$aliases[$route_name];
		$urlComplete = $reqUrl . $route ;
		$arr = [] ;

		foreach ($param_list as $value){
			$arr = implode("=",$value);
		}
		if (count($arr) == 0) {
			return $urlComplete ;
		}
		if (count($arr) > 1) {
			$controller = $urlComplete . "?" . implode("&amp;",$arr) ;
		}
		else{
			$controller = $urlComplete . "?" . $arr ;
		}
		
		return $controller ; 

	}

	public function addRoute($name, $url, $ctrl, $mth, $accLvl){
		
		/* 
		* Méthode addRoute : ajoute une route a la liste des routes 
		*
		* Paramètres :
		* 
		* - $name (String) : un nom pour la route
		* - $url (String)  : l'url de la route
		* - $ctrl (String) : le nom de la classe du Contrôleur 
		* - $mth (String)  : le nom de la méthode qui réalise la fonctionalité 
		*                     de la route
		*
		*
		* Algorithme :
		*
		* - Ajouter le tablau [ $ctrl, $mth ] au tableau self::$routes 
		*   sous la clé $url
		* - Ajouter la chaîne $url au tableau self::$aliases sous la clé $name
		*
		*/

		self::$routes[$url] = [$ctrl, $mth] ;
		self::$aliases[$name] = $url ;
		self::$level[$ctrl] = $accLvl ;
	}


	public function setDefaultRoute($url){

		/*
		* Méthode setDefaultRoute : fixe la route par défault
		* 
		* Paramètres :
		* 
		* - $url (String) : l'URL de la route par default
		*
		* Algorthme:
		*  
		* - ajoute $url au tableau self::$aliases sous la clé 'default'
		*
		*/

		self::$aliases['default'] = $url;

	}

}
