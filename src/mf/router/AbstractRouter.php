<?php

namespace mf\router;

abstract class AbstractRouter {

    /*   Une instance de HttpRequest */
    
    protected $http_req = null;
    
    /*
     * Attribut statique qui stocke les routes possibles de l'application 
     * 
     * - Une route est représentée par un tableau :
     *       [ le controlleur, la methode, niveau requis ]
     * 
     * - Chaque route est stokèe dans le tableau sous la clé qui est son URL 
     * 
     */
    
    static protected $routes = array ();

    /* 
     * Attribut statique qui stocke les alias des routes
     *
     * - Chaque URL est stocké dans une case ou la clé est son alias 
     *
     */

    static protected $aliases = array ();
    
    /*
     * Un constructeur 
     * 
     *  - initialiser l'attribut httpRequest
     * 
     */ 

    static protected $level = array ();

    public function __construct(){
        $this->http_req = new \mf\utils\HttpRequest();
    }
    
    
    abstract public function run();

    
    abstract public function urlFor($route_name, $param_list=[]);


    abstract public function setDefaultRoute($url);
   

    abstract public function addRoute($name, $url, $ctrl, $mth, $accLvl);

}
